# vgastream-rs

- High level VGA(0xB8000) library in freestanding Rust.
- Provides ```println``` ```print``` ```eprintln``` ```eprint``` macros.

## How to use

```sh

cargo add vgastream-rs


```

## Examples

```rust
/*main.rs*/

#![no_std]
#![no_main]

mod multiboot;
mod panic;

extern crate vgastream_rs;
use vgastream_rs::prelude::*;
use vga_rs::Color;

#[no_mangle]
pub extern "C" fn _start() -> ! {
	VgaOutStream::new().reset_with((Color::BLACK, Color::WHITE));

    let _ = main();
	loop{
		unsafe { panic::halt() }
	}
}

fn main() {
    println!("Hello, World!");
}


```

```rust
/*multiboot.rs*/

#[repr(C, align(4))]
pub struct MultiBoot {
    magic: u32,
    flags: u32,
    checksum: u32,
}

const ALIGN: u32 = 1 << 0;
const MEMINFO: u32 = 1 << 1;
const MAGIC: u32 = 0x1BADB002;
const FLAGS: u32 = ALIGN | MEMINFO;

#[used]
#[link_section = ".multiboot"]
#[no_mangle]
static multiboot: MultiBoot = MultiBoot {
    magic: MAGIC,
    flags: FLAGS,
    checksum: !(MAGIC + FLAGS)+1,
};

```
```rust
/*panic.rs*/

use core::panic::PanicInfo;
use vgastream_rs::prelude::*;

use core::arch::asm;
#[inline]
pub unsafe fn halt() -> ! {
    asm!("cli","hlt");
    core::hint::unreachable_unchecked()
}

#[panic_handler]
fn panic(info: &PanicInfo) -> ! {
    eprintln!("{}", info);
	loop
	{
		unsafe { halt() }
	}
}

```

## [simplekernel-rs](https://gitlab.com/hwoy/simplekernel-rs)
```sh
git clone https://gitlab.com/hwoy/simplekernel-rs.git
```
[simplekernel-rs](https://gitlab.com/hwoy/simplekernel-rs) can print out ```"Hello, World!"``` on x86 freestanding.

## Contact me

- Web: <https://github.com/hwoy>
- Email: <mailto:bosskillerz@gmail.com>
- Facebook: <https://www.facebook.com/watt.duean>
